# Files

This is how you create a file object:
```python
open("path/to/file", "<mode>")
```

## Modes

Mode describes how you will work with the file

| Mode | Name | Description |
| ----- | ----- | ----- |
| "r" | Read | Default value. Opens a file for reading, error if the file does not exist |
| "a" | Append | Opens a file for appending, creates the file if it does not exist |
| "w" | Write | Opens a file for writing, creates the file if it does not exist |
| "x" | Create | Creates the specified file, returns an error if the file exists |

There are additional modes that are declared additionally to the main modes

| Mode | Name | Description |
| ----- | ----- | ----- |
| "t" | Text | Default value. Text mode |
| "b" | Binary | Binary mode (e.g. images) |

## Read files

```python
file_obj = open("path/to/file", "r")

# Print all that file contains
print(file_obj.read())

# Print first 10 lines
print(file_obj.read(10))

# Read lines one by one:
print(file_obj.readline())
print(file_obj.readline())

# Iterate over lines in the file
for line in file_obj:
  print(line)

# Close the file
file_obj.close()
```

## Write to files
```python
with open("/path/to/file", "w") as file_obj:
    file_obj.write("This will replace everything that was in file before (if it existed)")

with open("/path/to/file", "a") as file_obj:
    file_obj.write("This adds a line to a file (and doesn't delete previous lines")
```