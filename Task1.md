# Create first Python program

## Call 'print' function
Type in your 'main.py' file the next line:

```python
print("Hello world!")
```

Then run the script in terminal like this:
```sh
python main.py
```

## Create first python function

Wrap your previous task in function.
Note that we don't use any '{' or '}' here, only tabs.
Each line in python function body will have exactly one tab at the beginning.
It also concerns if-else statements, loops etc.

```python
def print_hello_world():
    print("Hello world")
```

Note that when you call your script, it doesn't print a message any more.
Why? Because we need an entry point.

## Create entrypoint

Entry point in Python is just an 'if' statement with a special condition.
Add these lines to your script.

```python
if __name__ == "__main__":
    # By the way, this is how we declare comments in python
    # Call your function here:
    print_hello_world()
```
## Declare a variable

You don't need to write variable type in python

```python
name = "John"
age = 15
colors = ["red", "blue", "green"]
```

Main types in python: 
* str for string
* int for integer
* float 
* bool 


You can cast variables to another types:
```python
a = int("13")
b = str(10)
```

## Learn math operators

| Operator | Name |	Example |
| ------ | ------ | ------- |
| + | Addition | x + y |
| -	| Subtraction |	x - y |	
| *	| Multiplication	| x * y	|
| /	| Division |	x / y |
| %	| Modulus | x % y |
| ** |	Exponentiation | x ** y	| 
| // |	Floor division | x // y |

## Function with paramters

Our first function seems to be too simple, so let's create something a little more complicated

```python
# This function will greet a person by name
def greet(name):
    greeting_message = "Hello, {}".format(name)
    print(greeting_message)
```

Call this function in "main":
```python
if __name__ == "__main__":
    me = "Natalie"
    greet(me)
```
 Run main.py to see the result.

### Your turn!
1. Write a function that will return a sum of two numbers, given as arguments.
2. Write a function that will print information about person (name, age etc.) that is given in arguments.
3. Call these functions from 'main'.