# Python Demo

## Useful links

* [Python Docs](https://docs.python.org/3/tutorial/)
* [W3Schools Python Tutorial](https://www.w3schools.com/python/default.asp)
* [Python on Tutorialspoint](http://www.tutorialspoint.com/python)
* [Pandas](https://pandas.pydata.org/)
* [Pandas Short Introduction](https://pandas.pydata.org/pandas-docs/stable/10min.html)
* [Numpy](http://www.numpy.org/)
* [Numpy Short Tutorial](https://docs.scipy.org/doc/numpy-1.15.0/user/quickstart.html)
* [Jupyter](https://jupyter.org/)
* [PEP](https://www.python.org/dev/peps/)
* [Python: Notes for Professionals](https://books.goalkicker.com/PythonBook/)

## Hometask

Given an entity:
```sh
Client:
 id: int
 first_name: str
 last_name: str
 email: str
 gender: str
```

And file format: `csv` or `json`.

1. Create a few Python functions that will:
    * Insert a client record into the file
    * Read client data from file and print it in readable way
	* Get specific record from file (by id, for example)
    * Delete record from file (by id)
	
2. Bonus task: 
    * Implement methods given above using database instead of files