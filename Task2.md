# Special constructions in Python
## Conditions in Python

| Operator | Name | Example |
| ----- | ----- | ----- |
| == | Equals | x == y |
| != | Not Equals | x != y |
| < | Less Than | x < y |
| \> | Greater Than | x > y |
| <= | Less Than Or Equal  | x <= y |
| \>= | Greater Than Or Equal | x >= y |

Result of these operations is a bool (True or False).
These operators are often used in if...else statements and loops.

## If ... Else

```python
banana_count = 10
apple_count = 15

# This is a simple if statement (can be used without else)
# If statement 'banana_count > apple_count' is True, 
# then the message will be printed
if banana_count > apple_count:
    # Don't forget about tabs!
    print("There are more bananas that apples.")
    
# This is an if...else statement
if banana_count == apple_count:
    print("There are as many apples as bananas.")
else:
    # If statement 'banana_count == apple_count' equals to False,
    # this message will be printed
    print("There are different numbers of apples and bananas.")

# What if we have more than two possible options?
# Then we else 'elif' (short from 'else if')
# Note that event in this situation 'else' part is not mandatory
if banana_count > apple_count:
    print("There are more bananas than apples.")
elif banana_count < apple_count:
    print("There are more apples than bananas.")
else:
    print("There are equal number of apples and bananas.")
```

## While loop

With the while loop we can execute a set of statements as long as a condition is true.

```python
i = 0
while i < 10:
    print("Now i = {}".format(i))
    i += 1
```

## For loop

A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).
Often used for iterating through collections.

```python
colors = ["red", "orange", "yellow", "green", "blue", "purple"]
print("Here are the colors of the rainbow:")
for color in colors:
    print(color)
```