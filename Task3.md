# Collections
## Main collection types in Python
### List
* Ordered
* Allows duplicate members
* Changeable

```python
animals = ["cat", "dog", "mouse"]

# Print list
print(animals)

# Get element from list by index
print(animals[0])

# Change element in list
animals[2] = "rat"

# Add element to list
animals.append("tiger")

# Get amount of elements in list
len(animals)

# Iterating through the list
for animal in animals:
    print(animal)
    
# Check if element is in the list
if "dog" in animals:
  print("'dog' is in the list")

```

### Tuple
* Ordered
* Allows duplicate members
* Unchangeable

Actually similar to list, but you can't change its elements.
You can do similar operations as you did with list.

```python
tuple1 = ("one", "two", "three")
```

### Set
* Unordered
* Unindexed
* No duplicate members

```python
colors = {"red", "yellow", "orange"}

# Add element to the set
colors.add("blue")

# Iterate over elements in the set
for color in colors:
  print(color)
  
# Check if element is in the set
if "purple" in colors:
    print("'purple is in colors set")
```

### Dictionary
* Key-value pairs
* Unordered
* No duplicate keys
* Changeable
* Indexed

```python
person = {
    "name": "John Doe",
    "age": 34,
    "occupation": "Engineer"
}

# Get element by key
name = person["name"]

# Add pair to dictionary
person["nationality"] = "Australian"

# Iterate over keys and values in dictionary
for key, value in person.items():
    print("{} -> {}".format(key, value))
    
# Get only keys or values (that can also be iterated over)
keys = person.keys()
values = person.values()
```